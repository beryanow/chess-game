package chess;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class MoveControllerTest {
    @InjectMocks
    private MoveController moveController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMoves() {
        String result = moveController.getMoves();
        assertEquals("", result);
    }

    @Test
    public void saveMove() {
        moveController.saveMove("{\"move\":\"E4-E5\"}");
        String result = moveController.getMoves();
        assertEquals("E4-E5", result);

        moveController.saveMove("\"false_json\":\"\"}");
        result = moveController.getMoves();
        assertThat(result, equalTo("E4-E5"));
    }

    @Test
    public void clearMoves() {
        moveController.saveMove("{\"move\":\"E7-E8\"}");
        String result = moveController.getMoves();
        assertEquals("E7-E8", result);
        moveController.clearMove();
        result = moveController.getMoves();
        assertEquals("", result);
    }
}
