package chess;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.SpringApplication;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class BoardControllerTest {
    @InjectMocks
    private BoardController boardController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testBoard() {
        String result = boardController.getBoard();
        assertEquals("r_b,kn_b,b_b,q_b,k_b,b_b,kn_b,r_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b," +
                ".,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,p_w,p_w,p_w," +
                "p_w,p_w,p_w,p_w,p_w,r_w,kn_w,b_w,q_w,k_w,b_w,kn_w,r_w,1", result);
    }

    @Test
    public void testPlayer() {
        String result = String.valueOf(boardController.getPlayer());
        assertEquals("1", result);
    }

    @Test
    public void makeBoardChanges() {
        boardController.saveBoard("{\"body\":\"r_b\"}");
        String result = boardController.getBoard();
        assertEquals("r_b", result);

        boardController.clearBoard();
        boardController.saveBoard("\"really_false_json\":\"r_b\"}");
        result = boardController.getBoard();

        assertEquals("r_b,kn_b,b_b,q_b,k_b,b_b,kn_b,r_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b," +
                ".,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,p_w,p_w,p_w," +
                "p_w,p_w,p_w,p_w,p_w,r_w,kn_w,b_w,q_w,k_w,b_w,kn_w,r_w,1", result);

    }

    @Test
    public void makePlayerChanges() {
        SpringAndReactApplication.main(new String[]{});

        boardController.savePlayer("{\"player\": 2}");
        Long result = boardController.getPlayer();
        assertThat(result, equalTo(2L));

        boardController.savePlayer("{\"false_json\":\"\"}");
        result = boardController.getPlayer();
        assertThat(result, equalTo(null));

        boardController.savePlayer("\"really_false_json\":\"\"}");
        result = boardController.getPlayer();
        assertThat(result, equalTo(null));
    }
}


