package chess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.bind.annotation.*;


@RestController
public class BoardController {
    private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
    private static final String INITIAL_BOARD = "r_b,kn_b,b_b,q_b,k_b,b_b,kn_b,r_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b," +
            ".,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,p_w,p_w,p_w," +
            "p_w,p_w,p_w,p_w,p_w,r_w,kn_w,b_w,q_w,k_w,b_w,kn_w,r_w,1";
    private static final Long INITIAL_PLAYER = 1L;

    private String boardSetting = INITIAL_BOARD;
    private Long player = INITIAL_PLAYER;

    @GetMapping("/board")
    public String getBoard() {
        return boardSetting;
    }


    @GetMapping("/player")
    public Long getPlayer() {
        return player;
    }

    @PostMapping("/board/save")
    public void saveBoard(@RequestBody String request) {
        JSONParser jsonParser = new JSONParser();

        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) jsonParser.parse(request);
        } catch (ParseException e) {
            logger.info("Exception log: {}", e);
        }

        if (jsonObject != null) {
            boardSetting = (String) jsonObject.get("body");
        }
    }

    @PostMapping("/player/save")
    public void savePlayer(@RequestBody String request) {
        JSONParser jsonParser = new JSONParser();

        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) jsonParser.parse(request);
        } catch (ParseException e) {
            logger.info("Exception log: {}", e);
        }

        if (jsonObject != null) {
            player = (Long) jsonObject.get("player");
        }
    }

    @GetMapping("/board/clear")
    public void clearBoard() {
        boardSetting = INITIAL_BOARD;
    }
}
