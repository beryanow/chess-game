package chess;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class MoveController {
    private static final Logger logger = LoggerFactory.getLogger(MoveController.class);
    private ArrayList<String> moves = new ArrayList<>();

    @GetMapping("/moves")
    public String getMoves() {
        StringBuilder movesListBuilder = new StringBuilder("");
        for (int i = 0; i < moves.size(); i++) {
            movesListBuilder.append(moves.get(i));
            if (i % 2 == 1) {
                movesListBuilder.append("; ");
            } else if (i != moves.size() - 1) {
                movesListBuilder.append(", ");
            }
        }
        return movesListBuilder.toString();
    }

    @PostMapping("/moves/save")
    public void saveMove(@RequestBody String request) {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;

        try {
            jsonObject = (JSONObject) jsonParser.parse(request);
        } catch (ParseException e) {
            logger.info("Exception log: {}", e);
        }

        if (jsonObject != null) {
            moves.add((String) jsonObject.get("move"));
        }
    }

    @GetMapping("/moves/clear")
    public void clearMove() {
        moves.clear();
    }
}
