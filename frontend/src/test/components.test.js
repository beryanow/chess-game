import {expect} from 'chai';
import {mount, render, shallow, configure} from 'enzyme';

import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

configure({adapter: new Adapter()});

const axios = require("axios");
var MockAdapter = require("axios-mock-adapter");
var mock = new MockAdapter(axios);

import initializeBoard from "../main/components/initializeBoard";
import Rook from "../main/pieces/rook";
import Piece from "../main/pieces/piece";
import Knight from "../main/pieces/knight";
import Bishop from "../main/pieces/bishop";
import King from "../main/pieces/king";
import Queen from "../main/pieces/queen";
import Pawn from "../main/pieces/pawn";

describe('Piece Component', () => {
    let piece;

    beforeAll(() => {
        piece = new Piece(1);
    });

    it('Piece Stepwise Path Preparation Behaviour', () => {
        expect(JSON.stringify(piece.getSequence(6, 7))).to.be.equal(JSON.stringify([6, 7]));
    });

    it('Piece Stepwise Path Preparation Behaviour', () => {
        expect(JSON.stringify(piece.getSequence(7, 6))).to.be.equal(JSON.stringify([6, 7]));
    });
});

describe('Rook Component', () => {
    let rook;

    beforeAll(() => {
        rook = new Rook(1);
    });

    it('Vertical Move Behaviour', () => {
        expect(rook.isVertical(0, 2)).to.be.equal(false);
    });

    it('Horizontal Move Behaviour', () => {
        expect(rook.isHorizontal(9, 8)).to.be.equal(true);
    });

    it('Possible Move Behaviour', () => {
        expect(rook.isMovePossible(7, 8)).to.be.equal(false);
    });

    it('Check Behaviour', () => {
        expect(rook.isCheck(5, 6)).to.be.equal(true);
    });

    it('Explicit Possible Move List When Check Behaviour And Current Coord <= Desired Coordinate Behaviour', () => {
        expect(JSON.stringify(rook.getStepwisePathArray(0, 8, true))).to.be.equal(JSON.stringify([8]));
    });

    it('Explicit Possible Move List When Check And Current Coord > Desired Coordinate And One Lined Behaviour', () => {
        expect(JSON.stringify(rook.getStepwisePathArray(16, 0, true))).to.be.equal(JSON.stringify([8, 16]));
    });

    it('Explicit Possible Move List When Check And Not One Lined Behaviour', () => {
        expect(JSON.stringify(rook.getStepwisePathArray(15, 0, true))).to.be.equal(JSON.stringify([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]));
    });

    it('Overall Possible Move List When Every Move Is Valid Behaviour', () => {
        expect(JSON.stringify(rook.getPossibleMoves([1, 2, 3], 4))).to.be.equal(JSON.stringify([[2, 3, 4], [3, 4], [4]]));
    });

    it('Overall Possible Move List When At Least One Move Is Invalid Behaviour', () => {
        expect(JSON.stringify(rook.getPossibleMoves([1, 2, 3, 4, 6, 7], 8))).to.be.equal(JSON.stringify([]));
        expect(JSON.stringify(rook.getStepwisePath(15, 0, false))).to.be.equal(JSON.stringify([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]));
    });
});

describe('Knight Component', () => {
    let knight;

    beforeAll(() => {
        knight = new Knight(1);
    });

    it('Knight Move Behaviour', () => {
        expect(knight.isKnightStyle(1, 16)).to.be.equal(true);
        expect(knight.isKnightStyle(1, 17)).to.be.equal(false);
    });

    it('Possible Move Behaviour', () => {
        expect(knight.isMovePossible(1, 11)).to.be.equal(true);
    });

    it('Check Move Behaviour', () => {
        expect(knight.isCheck(1, 15)).to.be.equal(false);
    });

    it('Overall Possible Move List When Move Is Valid Behaviour', () => {
        expect(JSON.stringify(knight.getPossibleMoves([1], 11))).to.be.equal(JSON.stringify([[1]]));
    });

    it('Overall Stepwise Path List When Move Is Not Valid Behaviour', () => {
        expect(JSON.stringify(knight.getPossibleMoves([1], 12))).to.be.equal(JSON.stringify([]));
    });

    it('Overall Stepwise Path List Behaviour', () => {
        expect(JSON.stringify(knight.getStepwisePath())).to.be.equal(JSON.stringify([]));
    });
});

describe('Knight Component', () => {
    let bishop;

    beforeAll(() => {
        bishop = new Bishop(1);
    });

    it('Left Diagonal Move Behaviour', () => {
        expect(bishop.isLeftDiagonal(1, 10)).to.be.equal(true);
    });

    it('Right Diagonal Move Behaviour', () => {
        expect(bishop.isRightDiagonal(5, 12)).to.be.equal(true);
    });

    it('Possible Move Behaviour', () => {
        expect(bishop.isMovePossible(5, 11)).to.be.equal(false);
    });

    it('Check Move Behaviour', () => {
        expect(bishop.isCheck(5, 7)).to.be.equal(false);
    });

    it('Overall Possible Move List When Move Is Valid Behaviour', () => {
        expect(JSON.stringify(bishop.getPossibleMoves([2], 11))).to.be.equal(JSON.stringify([[11]]));
    });

    it('Overall Possible Move List When Move Is Not Valid Behaviour', () => {
        expect(JSON.stringify(bishop.getPossibleMoves([2], 12))).to.be.equal(JSON.stringify([]));
    });

    it('Overall Stepwise List When Move ', () => {
        expect(JSON.stringify(bishop.getStepwisePath([5], 12, false))).to.be.equal(JSON.stringify([]));
    });
});

describe('King Component', () => {
    let king;

    beforeAll(() => {
        king = new King(1);
    });

    it('King Move Behaviour', () => {
        expect(king.isKingStyle(1, 2)).to.be.equal(true);
        expect(king.isKingStyle(1, 9)).to.be.equal(true);
        expect(king.isKingStyle(1, 12)).to.be.equal(false);
    });

    it('Not Possible Move Behaviour', () => {
        expect(king.isMovePossible(1, 3)).to.be.equal(false);
    });

    it('Check Behaviour', () => {
        expect(king.isCheck()).to.be.equal(false);
    });

    it('Stepwise Behaviour', () => {
        expect(JSON.stringify(king.getStepwisePath([5], 12, false))).to.be.equal(JSON.stringify([]));
    });

    it('Overall Possible Moves Behaviour', () => {
        let squares = Array(64).fill(null);
        expect(JSON.stringify(king.getPossibleMoves([1, 2, 3], 4, squares))).to.be.equal(JSON.stringify([[5], [11], [12], [13]]));
        expect(JSON.stringify(king.getPossibleMoves([8, 7, 1], 8, squares))).to.be.equal(JSON.stringify([[9], [15], [0], [16], [17]]));
        squares[1] = new Rook(1);
        squares[9] = new Rook(1);
        expect(JSON.stringify(king.getPossibleMoves([8, 7, 2, 1], 8, squares))).to.be.equal(JSON.stringify([[15], [0], [16], [17]]));
    });
});

describe('Queen Component', () => {
    let queen;

    beforeAll(() => {
        queen = new Queen(1);
    });

    it('Queen Move Behaviour', () => {
        expect(queen.isMovePossible(7, 14)).to.be.equal(true);
        expect(queen.isMovePossible(7, 12)).to.be.equal(false);
    });

    it('Check Move Behaviour', () => {
        expect(queen.isCheck(0, 7)).to.be.equal(true);
        expect(queen.isCheck(15, 45)).to.be.equal(false);
    });

    it('Possible Move List Behaviour', () => {
        expect(JSON.stringify(queen.getPossibleMoves([1, 2, 3, 4, 5], 7))).to.be.equal(JSON.stringify([[2, 3, 4, 5, 6, 7], [3, 4, 5, 6, 7], [4, 5, 6, 7], [5, 6, 7], [6, 7]]));
    });

    it('Possible Move List Behaviour', () => {
        expect(JSON.stringify(queen.getPossibleMoves([1, 2, 3, 4, 5], 8))).to.be.equal(JSON.stringify([[8]]));
    });

    it('Possible Move List Behaviour', () => {
        expect(JSON.stringify(queen.getPossibleMoves([1, 2, 3], 17))).to.be.equal(JSON.stringify([[9, 17], [10, 17]]));
    });

    it('Possible Move List Behaviour', () => {
        expect(JSON.stringify(queen.getPossibleMoves([1, 2, 3], 19))).to.be.equal(JSON.stringify([[10, 19], [11, 19]]));
    });

    it('Overall Stepwise Path Behaviour', () => {
        expect(JSON.stringify(queen.getStepwisePath(45, 67))).to.be.equal(JSON.stringify([46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66]));
    });
});

describe('Board Component', () => {
    it('Rook Uninitialized On Board', () => {
        const squares = initializeBoard();
        expect(squares[0] instanceof Rook).to.be.equal(false);
    });

    it('Bishop Uninitialized On Board', () => {
        const squares = initializeBoard();
        expect(squares[0] instanceof Bishop).to.be.equal(false);
    });

    it('King Uninitialized On Board', () => {
        const squares = initializeBoard();
        expect(squares[0] instanceof King).to.be.equal(false);
    });
});

describe('Pawn Component', () => {
    let pawn;

    beforeAll(() => {
        pawn = new Pawn(2);
    });

    it('Pawn Move Behaviour', () => {
        expect(pawn.isMovePossible(9, 17, false)).to.be.equal(true);
    });

    it('Pawn Move Behaviour', () => {
        expect(pawn.isMovePossible(9, 16, false)).to.be.equal(false);
    });

    it('Pawn Move Behaviour', () => {
        expect(pawn.isMovePossible(9, 18, true)).to.be.equal(true);
    });

    it('Pawn Take Figure Move Behaviour', () => {
        expect(pawn.isMovePossible(9, 16, true)).to.be.equal(true);
        expect(pawn.isMovePossible(16, 32, true)).to.be.equal(false);
        expect((new Pawn(1)).isMovePossible(25, 16, true)).to.be.equal(true);
        expect((new Pawn(1)).isMovePossible(25, 17, true)).to.be.equal(false);
    });

    it('Pawn Move Behaviour', () => {
        expect(pawn.isCheck(16, 25)).to.be.equal(true);
        expect(pawn.isCheck(16, 23)).to.be.equal(true);
        expect(pawn.isCheck(16, 22)).to.be.equal(false);
        pawn.player = 3;
        expect(pawn.isCheck(16, 23)).to.be.equal(false);
    });

    it('Overall Stepwise Path When Check Behaviour', () => {
        expect(JSON.stringify(pawn.getStepwisePath(18, 2))).to.be.equal(JSON.stringify([10]));
        expect(JSON.stringify(pawn.getStepwisePath(2, 18))).to.be.equal(JSON.stringify([10]));
    });

    it('Overall Stepwise Path When Attempt To Checkmate Behaviour', () => {
        expect(JSON.stringify(pawn.getStepwisePathCheckmate(18, 2))).to.be.equal(JSON.stringify([2]));
        expect(JSON.stringify(pawn.getStepwisePathCheckmate(2, 18))).to.be.equal(JSON.stringify([18]));
        expect(JSON.stringify(pawn.getStepwisePathCheckmate(10, 2))).to.be.equal(JSON.stringify([2]));
        expect(JSON.stringify(pawn.getStepwisePathCheckmate(8, 16))).to.be.equal(JSON.stringify([16]));
        expect(JSON.stringify(pawn.getStepwisePathCheckmate(8, 17))).to.be.equal(JSON.stringify([]));
    });
});