import {expect} from 'chai';
import {mount, render, shallow, configure} from 'enzyme';

import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

configure({adapter: new Adapter()});

const axios = require("axios");
var MockAdapter = require("axios-mock-adapter");
var mock = new MockAdapter(axios);

import Game from '../main/components/game.js';
import Rook from "../main/pieces/rook";
import King from "../main/pieces/king";
import Knight from "../main/pieces/knight";
import Pawn from "../main/pieces/pawn";
import initializeBoard from "../main/components/initializeBoard";
import Piece from "../main/pieces/piece";
import Board from "../main/components/board";
import Queen from "../main/pieces/queen";
import Bishop from "../main/pieces/bishop";

describe('Game Component', () => {
    beforeAll(() => {
        mock.onGet("/board").reply(200,
            "r_b,kn_b,b_b,q_b,k_b,b_b,kn_b,r_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b,p_b," +
            ".,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,p_w,p_w,p_w," +
            "p_w,p_w,p_w,p_w,p_w,r_w,kn_w,b_w,q_w,k_w,b_w,kn_w,r_w,1");

        mock.onGet("/moves").reply(200,
            "E2-E4");

        mock.onGet("/player").reply(200,
            1);
    });

    it('Game Initialized Not Undefined', () => {
        const game = render(<Game/>);
        expect(game).not.to.be.equal(undefined);
    });

    it('Legal Move Behaviour', () => {
        const game = new Game();
        let squares = Array(64).fill(null);
        squares[5] = new Rook(1);
        squares[3] = new Rook(2);
        squares[2] = new Rook(1);
        game.setState({squares: squares});
        expect(game.isMoveLegal([1,2,3], 5)).to.be.equal(true);
        expect(game.isMoveLegal([1,2,3], 3)).to.be.equal(true);
    });

    it('Empty Path Behaviour', () => {
        const game = new Game();
        let squares = Array(64).fill(null);
        squares[5] = new Rook(1);
        squares[3] = new Rook(2);
        squares[2] = new Rook(1);
        expect(game.isPathEmpty([1,2,3], 5)).to.be.equal(false);
        expect(game.isPathEmpty([41], squares)).to.be.equal(true);
    });

    it('Check Observation Behaviour', () => {
        const game = new Game();
        let squares = Array(64).fill(null);
        squares[5] = new Rook(2);
        squares[62] = new Rook(2);
        squares[7] = new King(1);
        expect(JSON.stringify(game.isCheck(squares, 7))).to.be.equal(JSON.stringify([5]));
        squares[6] = new Knight(1);
        expect(JSON.stringify(game.isCheck(squares, 7))).to.be.equal(JSON.stringify([]));
    });

    it('Checkmate Observation Behaviour', () => {
        const game = new Game();
        let squares = Array(64).fill(null);
        squares[5] = new Rook(2);
        squares[7] = new King(1);
        squares[6] = new Knight(1);
        expect(game.isCheckMate(squares,[5], 7)).to.be.equal(false);
        squares[6] = null;
        squares[15] = new Pawn(1);
        expect(game.isCheckMate(squares,[5], 7)).to.be.equal(false);
    });

    it('Checkmate When No Space Observation Behaviour', () => {
        const game = new Game();
        let squares = Array(64).fill(null);
        squares[4] = new Rook(2);
        squares[6] = new King(1);
        squares[15] = new Pawn(1);
        squares[14] = new Pawn(1);
        squares[13] = new Pawn(1);
        squares[7] = new Knight(1);
        squares[22] = new Knight(1);
        expect(game.isCheckMate(squares,[4], 6)).to.be.equal(false);
    });

    it('Letter Line', () => {
        const game = new Game();
        expect(game.getLetter(8)).to.be.equal("A");
        expect(game.getLetter(9)).to.be.equal("B");
        expect(game.getLetter(10)).to.be.equal("C");
        expect(game.getLetter(11)).to.be.equal("D");
        expect(game.getLetter(12)).to.be.equal("E");
        expect(game.getLetter(13)).to.be.equal("F");
        expect(game.getLetter(14)).to.be.equal("G");
        expect(game.getLetter(15)).to.be.equal("H");
        expect(game.getLetter(-1)).to.be.equal("N/A");
    });

    it('Number Line', () => {
        const game = new Game();
        expect(game.getNumber(7)).to.be.equal("8");
        expect(game.getNumber(15)).to.be.equal("7");
        expect(game.getNumber(23)).to.be.equal("6");
        expect(game.getNumber(31)).to.be.equal("5");
        expect(game.getNumber(39)).to.be.equal("4");
        expect(game.getNumber(47)).to.be.equal("3");
        expect(game.getNumber(55)).to.be.equal("2");
        expect(game.getNumber(63)).to.be.equal("1");
        expect(game.getNumber(100)).to.be.equal("N/A");
    });

    it('Game Handle Click', () => {
        let game = shallow(<Game />);
        game.find('button').at(0).simulate('click');
        game.find('button').at(1).simulate('click');

        const squares = Array(64).fill(null);
        for (let i = 8; i < 16; i++) {
            squares[i] = new Pawn(2);
        }
        for (let i = 48; i < 56; i++) {
            squares[i] = new Pawn(1);
        }
        squares[0] = new Rook(2);
        squares[7] = new Rook(2);
        squares[56] = new Rook(1);
        squares[63] = new Rook(1);
        squares[1] = new Knight(2);
        squares[6] = new Knight(2);
        squares[57] = new Knight(1);
        squares[62] = new Knight(1);
        squares[2] = new Bishop(2);
        squares[5] = new Bishop(2);
        squares[58] = new Bishop(1);
        squares[61] = new Bishop(1);
        squares[3] = new Queen(2);
        squares[59] = new Queen(1);
        squares[4] = new King(2);
        squares[60] = new King(1);

        game.setState({sourceSelection: -1, squares: squares});
        game.find('button').at(2).simulate('click');
        game.setState({sourceSelection: 1});
        game.find('button').at(3).simulate('click');
        expect(game).not.to.be.equal(undefined);
    });

    it('Game Short Castle Process', () => {
        let game = shallow(<Game />);
        const squares = Array(64).fill(null);

        squares[60] = new King(1);
        squares[63] = new Rook(1);

        game.setState({sourceSelection: -1, squares: squares});
        game.find('button').at(4).simulate('click');
        game.setState({sourceSelection: 60});
        game.find('button').at(5).simulate('click');
        expect(game).not.to.be.equal(undefined);
    });

    it('Game Long Castle Process', () => {
        let game = shallow(<Game />);
        const squares = Array(64).fill(null);

        squares[60] = new King(1);
        squares[56] = new Rook(1);

        game.setState({sourceSelection: -1, squares: squares});
        game.find('button').at(4).simulate('click');
        game.setState({sourceSelection: 60});
        game.find('button').at(6).simulate('click');
        expect(game).not.to.be.equal(undefined);
    });

    it('Game King Move', () => {
        let game = shallow(<Game />);
        const squares = Array(64).fill(null);

        squares[60] = new King(1);

        game.setState({sourceSelection: -1, squares: squares});
        game.find('button').at(4).simulate('click');
        game.setState({sourceSelection: 60});
        game.find('button').at(7).simulate('click');
        expect(game).not.to.be.equal(undefined);
    });
});