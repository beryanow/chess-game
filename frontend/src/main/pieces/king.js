import Piece from './piece.js';

export default class King extends Piece {
    constructor(player) {
        super(player, (player === 1 ? "/piece_images/king_white.png" : "/piece_images/king_black.png"));
    }

    getPossibleMoves(stepwisePathToKing, current, squares) {
        let paths = [];
        const values = [1, 7, 8, 9];

        for (let i = 0; i < values.length; i++) {
            if (stepwisePathToKing.indexOf(current - values[i]) === -1 && !squares[current - values[i]] && (current - values[i] >= 0)) {
                paths.push([current - values[i]]);
            }

            if (stepwisePathToKing.indexOf(current + values[i]) === -1 && !squares[current + values[i]] && (current + values[i] <= 63)) {
                paths.push([current + values[i]]);
            }
        }

        return paths;
    }

    isKingStyle(pointFrom, pointTo) {
        return (pointFrom - 9 === pointTo) ||
            (pointFrom - 8 === pointTo) ||
            (pointFrom - 7 === pointTo) ||
            (pointFrom - 1 === pointTo) ||
            (pointFrom + 9 === pointTo) ||
            (pointFrom + 8 === pointTo) ||
            (pointFrom + 7 === pointTo) ||
            (pointFrom + 1 === pointTo);
    }

    isMovePossible(previous, forward) {
        return this.isKingStyle(previous, forward);
    }

    isCheck() {
        return false;
    }

    getStepwisePath() {
        return [];
    }
}