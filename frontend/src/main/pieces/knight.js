import Piece from './piece.js';

export default class Knight extends Piece {
    constructor(player) {
        super(player, (player === 1 ? "/piece_images/knight_white.png" : "/piece_images/knight_black.png"));
    }

    isKnightStyle(pointFrom, pointTo) {
        return (pointFrom - 17 === pointTo) ||
            (pointFrom - 10 === pointTo) ||
            (pointFrom - 6 === pointTo) ||
            (pointFrom - 15 === pointTo) ||
            (pointFrom + 15 === pointTo) ||
            (pointFrom + 6 === pointTo) ||
            (pointFrom + 10 === pointTo) ||
            (pointFrom + 17 === pointTo);
    }

    isMovePossible(previous, forward) {
        return this.isKnightStyle(previous, forward);
    }

    isCheck(current, king) {
        return this.isKnightStyle(current, king);
    }

    getPossibleMoves(stepwisePathToKing, current) {
        let paths = [];

        for (let i = 0; i < stepwisePathToKing.length; i++) {
            if (this.isMovePossible(stepwisePathToKing[i], current)) {
                paths.push([stepwisePathToKing[i]]);
            }
        }

        return paths;
    }

    getStepwisePath() {
        return [];
    }
}