import Piece from './piece.js';

export default class Bishop extends Piece {
    constructor(player) {
        super(player, (player === 1 ? "/piece_images/bishop_white.png" : "/piece_images/bishop_black.png"), "bishop");
    }

    isLeftDiagonal(previous, forward) {
        return Math.abs(previous - forward) % 9 === 0;
    }

    isRightDiagonal(previous, forward) {
        return Math.abs(previous - forward) % 7 === 0;
    }

    isMovePossible(previous, forward) {
        return this.isLeftDiagonal(previous, forward) || this.isRightDiagonal(previous, forward);
    }

    isCheck(current, king) {
        return this.isLeftDiagonal(current, king) || this.isRightDiagonal(current, king);
    }

    getPossibleMoves(stepwisePathToKing, current) {
        let paths = [];

        for (let i = 0; i < stepwisePathToKing.length; i++) {
            if (this.isMovePossible(stepwisePathToKing[i], current)) {
                paths.push(this.getStepwisePathCheckmate(stepwisePathToKing[i], current));
            }
        }

        return paths;
    }

    getStepwisePathArray(previous, forward, checkmate) {
        let start, end, increment;
        start = this.getSequence(previous, forward)[0];
        end = this.getSequence(previous, forward)[1];

        if (Math.abs(previous - forward) % 9 === 0) {
            increment = 9;
            start += 9;
        } else {
            increment = 7;
            start += 7;
        }

        return this.createArray(start, end, increment, checkmate);
    }

    getStepwisePath(previous, forward) {
        return this.getStepwisePathArray(previous, forward, false);
    }

    getStepwisePathCheckmate(previous, forward) {
        return this.getStepwisePathArray(previous, forward, true);
    }
}




