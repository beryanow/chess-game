import React from 'react';

export default class Piece extends React.Component  {
    constructor(player, iconUrl, type) {
        super(undefined);
        this.player = player;
        this.style = {backgroundImage: "url('" + iconUrl + "')", backgroundSize: "100% 100%"};
        this.type = type;
    }

    createArray(start, end, increment, checkmate) {
        let stepwisePath = [];

        if (!checkmate) {
            for (let i = start; i < end; i += increment) {
                stepwisePath.push(i);
            }
        } else {
            for (let i = start; i <= end; i += increment) {
                stepwisePath.push(i);
            }
        }

        return stepwisePath;
    }

    getSequence(previous, forward) {
        if (previous > forward) {
            return [forward, previous];
        } else {
            return [previous, forward];
        }
    }
}