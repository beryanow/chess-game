import Piece from './piece.js';

export default class Queen extends Piece {
    constructor(player) {
        super(player, (player === 1 ? "/piece_images/queen_white.png" : "/piece_images/queen_black.png"));
    }

    isLeftDiagonal(previous, forward) {
        return Math.abs(previous - forward) % 9 === 0;
    }

    isRightDiagonal(previous, forward) {
        return Math.abs(previous - forward) % 7 === 0;
    }

    isVertical(previous, forward) {
        return Math.abs(previous - forward) % 8 === 0;
    }

    isHorizontal(previous, forward) {
        return (forward >= (previous - previous % 8)) && (forward <= previous + (7 - previous % 8));
    }

    isMovePossible(previous, forward) {
        return (this.isLeftDiagonal(previous, forward) || this.isRightDiagonal(previous, forward)) || // like bishop
            (this.isVertical(previous, forward) || this.isHorizontal(previous, forward)); // like rook
    }

    isCheck(current, king) {
        return (this.isLeftDiagonal(current, king) || this.isRightDiagonal(current, king)) || // like bishop
            this.isVertical(current, king) || this.isHorizontal(current, king); // like rook
    }

    getPossibleMoves(stepwisePathToKing, current) {
        let paths = [];

        for (let i = 0; i < stepwisePathToKing.length; i++) {
            if (this.isMovePossible(stepwisePathToKing[i], current)) {
                paths.push(this.getStepwisePathCheckmate(stepwisePathToKing[i], current));
            }
        }

        return paths;
    }

    getStepwisePathArray(previous, forward, checkmate) {
        let start, end, increment;

        const sequence = this.getSequence(previous, forward);
        start = sequence[0];
        end = sequence[1];

        if (this.isVertical(previous, forward)) {
            increment = 8;
            start += 8;
        } else if (this.isLeftDiagonal(previous, forward)) {
            // left diagonal
            increment = 9;
            start += 9;
        } else if (this.isRightDiagonal(previous, forward)) {
            // right diagonal
            increment = 7;
            start += 7;
        } else {
            increment = 1;
            start += 1;
        }

        return this.createArray(start, end, increment, checkmate);
    }

    getStepwisePath(previous, forward) {
        return this.getStepwisePathArray(previous, forward, false);
    }

    getStepwisePathCheckmate(previous, forward) {
        return this.getStepwisePathArray(previous, forward, true);
    }
}