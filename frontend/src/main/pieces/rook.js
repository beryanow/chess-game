import Piece from './piece.js';

export default class Rook extends Piece {
    constructor(player) {
        super(player, (player === 1 ? "/piece_images/rook_white.png" : "/piece_images/rook_black.png"));
    }

    isVertical(previous, forward) {
        return Math.abs(previous - forward) % 8 === 0;
    }

    isHorizontal(previous, forward) {
        return (forward >= (previous - previous % 8)) && (forward <= previous + (7 - previous % 8));
    }

    isMovePossible(previous, forward) {
        return (this.isVertical(previous, forward)) ||  // vertical line check
            (this.isHorizontal(previous, forward)); // horizontal line check
    }

    isCheck(current, king) {
        return (this.isVertical(current, king)) ||  // vertical line check
            (this.isHorizontal(current, king) ); // horizontal line check
    }

    getPossibleMoves(stepwisePathToKing, current) {
        let paths = [];

        for (let i = 0; i < stepwisePathToKing.length; i++) {
            if (this.isMovePossible(stepwisePathToKing[i], current)) {
                paths.push(this.getStepwisePathCheckmate(stepwisePathToKing[i], current));
            }
        }

        return paths;
    }

    getStepwisePathArray(previous, forward, checkmate) {
        let start, end, increment;

        if (previous > forward) {
            start = forward;
            end = previous;
        } else {
            start = previous;
            end = forward;
        }

        if (Math.abs(previous - forward) % 8 === 0) {
            // vertical path
            increment = 8;
            start += 8;
        } else {
            // horizontal path
            increment = 1;
            start += 1;
        }

        return this.createArray(start, end, increment, checkmate);
    }

    getStepwisePath(previous, forward) {
        return this.getStepwisePathArray(previous, forward, false);
    }

    getStepwisePathCheckmate(previous, forward) {
        return this.getStepwisePathArray(previous, forward, true);
    }
}