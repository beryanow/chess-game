import React from 'react';

import '../index.css';
import Board from './board.js';
import initializeBoard from "./initializeBoard";
import King from "../pieces/king.js";
import Rook from "../pieces/rook";
import Knight from "../pieces/knight";
import Bishop from "../pieces/bishop";
import Queen from "../pieces/queen";
import Pawn from "../pieces/pawn";

const axios = require("axios");

export default class Game extends React.Component {
    constructor() {
        super();

        let player = 1;
        axios.get('/player').then(response => {
            player = response.data;
        });

        let structure = initializeBoard();
        this.state = {
            squares: structure,
            sourceSelection: -1,
            whiteKing: 60,
            blackKing: 4,
            player: player,
            borders: [0, 1, 2, 3, 4, 5, 6, 7, 56, 57, 58, 59, 60, 61, 62, 63,
                15, 23, 31, 39, 47, 55, 8, 16, 24, 32, 40, 48]
        };

        setTimeout(() => {
            this.setState({squares: this.state.squares.slice(), player: player});
        }, 500)
    }

    isMoveLegal(srcToDestPath, forward) {
        let isLegal = true;
        for (let i = 0; i < srcToDestPath.length; i++) {
            if (this.state.squares[srcToDestPath[i]] !== null) {
                isLegal = false;
            }
        }

        if (this.state.squares[forward] !== null && this.state.squares[forward].player === this.state.player) {
            isLegal = false;
        }

        return isLegal;
    }

    isPathEmpty(src, squares) {
        let isLegal = true;
        let flag = -1;

        for (let i = 0; i < src.length; i++) {
            if (this.state.borders.indexOf(src[i]) !== -1) {
                if (flag === 1) {
                    return false;
                }
                flag = 1;
            }
            if (squares[src[i]] !== null) {
                isLegal = false;
            }
        }

        return isLegal;
    }

    isCheck(squares, king) {
        let threats = [];

        for (let i = 0; i < 64; i++) {
            if (squares[i] !== null) {
                if (this.state.player !== squares[i].player) {
                    if (squares[i].isCheck(i, king)) {
                        threats.push(i);
                    }
                }
            }
        }

        let editThreats = threats.slice();

        for (let i = 0; i < threats.length; i++) {
            const stepWisePathToKing = squares[threats[i]].getStepwisePath(threats[i], king);
            if (!this.isPathEmpty(stepWisePathToKing, squares)) {
                editThreats.splice(editThreats.indexOf(threats[i]), 1);
            }
        }

        return editThreats;
    }

    getNumber(position) {
        switch (Math.floor(position / 8)) {
            case 0:
                return "8";
            case 1:
                return "7";
            case 2:
                return "6";
            case 3:
                return "5";
            case 4:
                return "4";
            case 5:
                return "3";
            case 6:
                return "2";
            case 7:
                return "1";
            default:
                return "N/A"
        }
    }

    getLetter(previous) {
        switch (previous % 8) {
            case 0:
                return "A";
            case 1:
                return "B";
            case 2:
                return "C";
            case 3:
                return "D";
            case 4:
                return "E";
            case 5:
                return "F";
            case 6:
                return "G";
            case 7:
                return "H";
            default:
                return "N/A"
        }
    }
    isCheckMate(squares, threats, king) {
        let candidates = [];

        for (let i = 0; i < 64; i++) {
            if (squares[i] !== null) {
                if (this.state.player === squares[i].player) {

                    for (let m = 0; m < threats.length; m++) {
                        if (squares[i] instanceof King) {
                            const stepwisePathToKing = squares[threats[m]].getStepwisePath(threats[m], king);

                            const possibleMoves = squares[i].getPossibleMoves(stepwisePathToKing, i, squares);
                            for (let b = 0; b < possibleMoves.length; b++) {
                                if (possibleMoves[b].length !== 0) {
                                    candidates.push([i, possibleMoves[b][0]]);
                                }
                            }
                            continue;
                        }

                        if (squares[i].isCheck(i, threats[m])) {
                            const stepwisePathToThreat = squares[i].getStepwisePath(i, threats[m]);
                            if (this.isPathEmpty(stepwisePathToThreat, squares)) {
                                candidates.push([i, threats[m]]);
                            }
                        }

                        const stepwisePathToKing = squares[threats[m]].getStepwisePath(threats[m], king);

                        const possibleMoves = squares[i].getPossibleMoves(stepwisePathToKing, i, squares);
                        for (let b = 0; b < possibleMoves.length; b++) {
                            if (this.isPathEmpty(possibleMoves[b], squares) && possibleMoves[b].length !== 0) {
                                let last = possibleMoves[b].length - 1;
                                candidates.push([i, possibleMoves[b][last]]);
                            }
                        }
                    }
                }
            }
        }

        const editCandidates = candidates.slice();

        for (let i = 0; i < candidates.length; i++) {
            if (squares[candidates[i][0]] instanceof King) {
                const previous = candidates[i][0];
                const forward = candidates[i][1];

                const newSquares = squares.slice();
                newSquares[forward] = newSquares[previous];
                newSquares[previous] = null;

                const newThreats = this.isCheck(newSquares, forward);
                if (newThreats.length > 0) {
                    editCandidates.splice(editCandidates.indexOf(i), 1);
                }
                continue;
            }

            const previous = candidates[i][0];
            const forward = candidates[i][1];

            const newSquares = squares.slice();
            newSquares[forward] = newSquares[previous];
            newSquares[previous] = null;

            const newThreats = this.isCheck(newSquares, king);
            if (newThreats.length > 0) {
                editCandidates.splice(editCandidates.indexOf(i), 1);
            }
        }

        return editCandidates.length === 0;
    }

    getBoard(squares) {
        let newBoard = "";
        for (let i = 0; i < squares.length; i++) {
            if (squares[i] === null) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += ".";
            } else if (squares[i] instanceof Rook && squares[i].player === 1) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "r_w";
            } else if (squares[i] instanceof Rook && squares[i].player === 2) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "r_b";
            } else if (squares[i] instanceof Knight && squares[i].player === 1) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "kn_w";
            } else if (squares[i] instanceof Knight && squares[i].player === 2) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "kn_b";
            } else if (squares[i] instanceof Bishop && squares[i].player === 1) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "b_w";
            } else if (squares[i] instanceof Bishop && squares[i].player === 2) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "b_b";
            } else if (squares[i] instanceof Queen && squares[i].player === 1) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "q_w";
            } else if (squares[i] instanceof Queen && squares[i].player === 2) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "q_b";
            } else if (squares[i] instanceof King && squares[i].player === 1) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "k_w";
            } else if (squares[i] instanceof King && squares[i].player === 2) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "k_b";
            } else if (squares[i] instanceof Pawn && squares[i].player === 1) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "p_w";
            } else if (squares[i] instanceof Pawn && squares[i].player === 2) {
                if (newBoard !== "") {
                    newBoard += ",";
                }
                newBoard += "p_b";
            }
        }
        return newBoard;
    }

    handleClick(i) {
        if (this.state.sourceSelection === -1) {
            const squares = this.state.squares.slice();

            let king;
            if (this.state.player === 1) {
                king = this.state.whiteKing;
            } else if (this.state.player === 2) {
                king = this.state.blackKing;
            }

            const threats = this.isCheck(squares, king);

            if (threats.length > 0) {
                if (this.isCheckMate(squares, threats, king)) {
                    alert("CHECKMATE");
                    return;
                }
            }

            if (this.state.squares[i] && (this.state.squares[i].player === this.state.player)) {
                const squares = this.state.squares.slice();
                squares[i].style = {...this.state.squares[i].style, backgroundColor: "#3F4077"};

                this.setState({squares: squares, sourceSelection: i});
            }
        } else {
            let squares = this.state.squares.slice();
            const previous = this.state.sourceSelection;

            squares[previous].style = {...this.state.squares[previous].style, backgroundColor: null};

            this.setState({squares: squares});

            const beat = !!squares[i];
            const isMovePossible = squares[previous].isMovePossible(previous, i, beat);
            const stepwisePath = squares[previous].getStepwisePath(previous, i);
            const isMoveLegal = this.isMoveLegal(stepwisePath, i);

            const checkSquares = squares.slice();

            let castle = 0;
            if (checkSquares[previous] instanceof King && (previous === 60 || previous === 4) && checkSquares[previous + 1] === null && checkSquares[previous + 3] instanceof Rook && i === previous + 2) {
                checkSquares[i] = checkSquares[previous];
                checkSquares[previous] = null;
                checkSquares[previous + 1] = checkSquares[previous + 3];
                checkSquares[previous + 3] = null;
                axios.post("/moves/save", {
                    move: "0-0"
                });
                castle = 1;
            }

            if (checkSquares[previous] instanceof King && (previous === 60 || previous === 4) && checkSquares[previous - 1] === null && checkSquares[previous - 2] === null && checkSquares[previous - 3] === null && checkSquares[previous - 4] instanceof Rook && i === previous - 2) {
                checkSquares[i] = checkSquares[previous];
                checkSquares[previous] = null;
                checkSquares[previous - 1] = checkSquares[previous - 4];
                checkSquares[previous - 4] = null;
                axios.post("/moves/save", {
                    move: "0-0-0"
                });
                castle = 1;
            }

            if (castle === 1) {
                if (this.state.player === 1) {
                    this.setState({whiteKing: i});
                } else if (this.state.player === 2) {
                    this.setState({blackKing: i});
                }

                let newBoard = this.getBoard(checkSquares);

                axios.post("/board/save", {
                    body: newBoard
                });

                let player = this.state.player === 1 ? 2 : 1;

                axios.post("/player/save", {
                    player: player
                });

                this.setState({sourceSelection: -1, squares: checkSquares, player: player});
                return;
            }

            if (isMovePossible && isMoveLegal) {
                const checkSquares = squares.slice();

                let king;
                if (this.state.player === 1) {
                    king = this.state.whiteKing;
                } else if (this.state.player === 2) {
                    king = this.state.blackKing;
                }

                let startLetter = this.getLetter(previous);
                let startNumber = this.getNumber(previous);

                let endLetter = this.getLetter(i);
                let endNumber = this.getNumber(i);

                axios.post("/moves/save", {
                    move: startLetter + startNumber + " - " + endLetter + endNumber
                });

                checkSquares[i] = checkSquares[previous];
                checkSquares[previous] = null;

                if (checkSquares[i] instanceof Pawn && i >= 0 && i <= 7) {
                    checkSquares[i] = new Queen(1);
                } else if (checkSquares[i] instanceof Pawn && i >= 56 && i <= 63) {
                    checkSquares[i] = new Queen(2);
                }

                let tempKing = -1;
                if (checkSquares[i] instanceof King) {
                    if (this.state.player === 1) {
                        tempKing = this.state.whiteKing;
                        this.setState({whiteKing: i});
                    } else if (this.state.player === 2) {
                        tempKing = this.state.blackKing;
                        this.setState({blackKing: i});
                    }

                    king = i;
                }

                const threats = this.isCheck(checkSquares, king);
                if (!threats.length > 0) {
                    squares = checkSquares;
                } else {

                    this.setState({sourceSelection: -1, squares: squares});
                    if (tempKing !== -1) {
                        if (this.state.player === 1) {
                            this.setState({whiteKing: tempKing});
                        } else if (this.state.player === 2) {
                            this.setState({blackKing: tempKing});
                        }
                    }
                    return;
                }

                let player = this.state.player === 1 ? 2 : 1;

                this.setState({sourceSelection: -1, squares: squares, player: player});

                let newBoard = this.getBoard(squares);

                axios.post("/player/save", {
                    player: player
                });

                axios.post("/board/save", {
                    body: newBoard
                });
            } else {
                this.setState({sourceSelection: -1, squares: squares});
            }
        }
    }

    handleButtonClick() {
        axios.get('/board/clear');
        axios.get('/moves/clear');
        axios.post("/player/save", {
            player: 1
        });
        window.location.reload();
    }

    handleHistoryButtonClick() {
        axios.get('/moves').then(response => {
            alert(response.data);
        });
    }

    render() {
        return (
            <div>
                <div className="game">
                    <div className="game-board">
                        <Board squares={this.state.squares}
                               onClick={(i) => this.handleClick(i)}/>
                    </div>
                    <div>
                        <ul className="ul">
                            <li className= "li">
                                <button type="submit" onClick={this.handleButtonClick}
                                        style={{border: 0, background: "transparent"}}>
                                    <img src="/control_images/clear_board.png" width="50" height="50" alt="submit"/>
                                </button>
                            </li>
                            <li className= "li">
                                <button type="submit" onClick={this.handleHistoryButtonClick}
                                        style={{border: 0, background: "transparent"}}>
                                    <img src="/control_images/watch_history.png" width="50" height="50" alt="submit"/>
                                </button>
                            </li>
                            <li className= "li">
                                <button type="submit" value="Disabled" onClick={() => this.handleClick(1)}
                                        style={{border: 0, background: "transparent", opacity: 0}}>
                                    <img width="1" height="1" alt="submit"/>
                                </button>
                            </li>
                            <li className= "li">
                                <button type="submit" value="Disabled" onClick={() => this.handleClick(18)}
                                        style={{border: 0, background: "transparent", opacity: 0}}>
                                    <img width="1" height="1" alt="submit"/>
                                </button>
                            </li>
                            <li className= "li">
                                <button type="submit" value="Disabled" onClick={() => this.handleClick(60)}
                                        style={{border: 0, background: "transparent", opacity: 0}}>
                                    <img width="1" height="1" alt="submit"/>
                                </button>
                            </li>
                            <li className= "li">
                                <button type="submit" value="Disabled" onClick={() => this.handleClick(62)}
                                        style={{border: 0, background: "transparent", opacity: 0}}>
                                    <img width="1" height="1" alt="submit"/>
                                </button>
                            </li>
                            <li className= "li">
                                <button type="submit" value="Disabled" onClick={() => this.handleClick(58)}
                                        style={{border: 0, background: "transparent", opacity: 0}}>
                                    <img width="1" height="1" alt="submit"/>
                                </button>
                            </li>
                            <li className= "li">
                                <button type="submit" value="Disabled" onClick={() => this.handleClick(61)}
                                        style={{border: 0, background: "transparent", opacity: 0}}>
                                    <img width="1" height="1" alt="submit"/>
                                </button>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        );
    }
}
