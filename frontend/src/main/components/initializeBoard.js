import Bishop from '../pieces/bishop.js';
import King from '../pieces/king.js';
import Knight from '../pieces/knight.js';
import Pawn from '../pieces/pawn.js';
import Queen from '../pieces/queen.js';
import Rook from '../pieces/rook.js';

const axios = require("axios");

export default function initializeBoard(test) {
    const squares = Array(64).fill(null);
    axios.get('/board').then(response => {
        for (let i = 0; i < response.data.split(",").length; i++) {
            switch (response.data.split(",")[i]) {
                case "p_w":
                    squares[i] = new Pawn(1);
                    break;
                case "r_w":
                    squares[i] = new Rook(1);
                    break;
                case "kn_w":
                    squares[i] = new Knight(1);
                    break;
                case "b_w":
                    squares[i] = new Bishop(1);
                    break;
                case "k_w":
                    squares[i] = new King(1);
                    break;
                case "q_w":
                    squares[i] = new Queen(1);
                    break;
                case "p_b":
                    squares[i] = new Pawn(2);
                    break;
                case "r_b":
                    squares[i] = new Rook(2);
                    break;
                case "kn_b":
                    squares[i] = new Knight(2);
                    break;
                case "b_b":
                    squares[i] = new Bishop(2);
                    break;
                case "k_b":
                    squares[i] = new King(2);
                    break;
                case "q_b":
                    squares[i] = new Queen(2);
                    break;
                default:
                    break;
            }
        }
    }).catch(err => {});

    return squares;
}
