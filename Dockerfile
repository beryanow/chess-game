FROM node:latest
COPY . /chess

WORKDIR /chess/frontend
RUN npm i react-dev-utils

WORKDIR /chess
RUN apt-get update
RUN apt-get install openjdk-8-jdk -y
RUN apt-get install maven -y

ENTRYPOINT mvn spring-boot:run
